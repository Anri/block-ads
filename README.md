# Liste de filtres pour bloqueur de pubs

## [uBlock Origin](https://ublockorigin.com/)

- Tableau de bord → Listes de filtres → Vos propres listes (en bas)
  - Ajoutez : `https://git.mylloon.fr/Anri/block-ads/raw/branch/master/filters.txt`

### Recommandations dans les listes par défaut

- Tout cocher dans l'onglet `Bannières de cookie` (Listes de filtres → Bannières de cookie)
  > A pour effet de désactiver beaucoup de bannières de cookies
- Cocher la case `AdGuard URL Tracking Protection` (Listes de filtres → Confidentialité)
  > Retire le tracking dans les URL (remplaçant une partie de [ClearURLs](https://chrome.google.com/webstore/detail/clearurls/lckanjgmijmafbedllaakclkaicjfmnk))

### Listes tierces

- [`https://raw.githubusercontent.com/DandelionSprout/adfilt/master/LegitimateURLShortener.txt`](https://raw.githubusercontent.com/DandelionSprout/adfilt/master/LegitimateURLShortener.txt)
  > Remplace une partie de [ClearURLs](https://chrome.google.com/webstore/detail/clearurls/lckanjgmijmafbedllaakclkaicjfmnk)
- [`https://raw.githubusercontent.com/DandelionSprout/adfilt/master/ClearURLs%20for%20uBo/clear_urls_uboified.txt`](https://raw.githubusercontent.com/DandelionSprout/adfilt/master/ClearURLs%20for%20uBo/clear_urls_uboified.txt)
  > Ajoutant + de règles et afin de remplacé ClearURLs
- [`https://www.i-dont-care-about-cookies.eu/abp/`](https://www.i-dont-care-about-cookies.eu/abp/)
  > Remplaçant [I still don't care about cookies](https://github.com/OhMyGuus/I-Still-Dont-Care-About-Cookies)
- [`https://gitflic.ru/project/magnolia1234/bypass-paywalls-clean-filters/blob/raw?file=bpc-paywall-filter.txt`](https://gitflic.ru/project/magnolia1234/bypass-paywalls-clean-filters/blob/raw?file=bpc-paywall-filter.txt)
  > Permet de supprimer certains paywalls

## Résumé

Liste de tout ce qui est cité plus haut :

```
https://git.mylloon.fr/Anri/block-ads/raw/branch/master/filters.txt
https://raw.githubusercontent.com/DandelionSprout/adfilt/master/LegitimateURLShortener.txt
https://raw.githubusercontent.com/DandelionSprout/adfilt/master/ClearURLs%20for%20uBo/clear_urls_uboified.txt
https://www.i-dont-care-about-cookies.eu/abp/
https://gitflic.ru/project/magnolia1234/bypass-paywalls-clean-filters/blob/raw?file=bpc-paywall-filter.txt
```
